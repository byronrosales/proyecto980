from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import formularioRegistrar
from .models import User, mascota

admin.site.register(User, UserAdmin)
admin.site.register(mascota)

# Register your models here.
