from django.db import models
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth.models import AbstractUser


class User (AbstractUser):
	
	telefono = models.CharField(max_length = 20)
	edad = models.CharField(max_length = 20)
	rol = models.CharField(max_length = 20, default="cliente")
	numero_intentos = models.IntegerField(default=0)
	
class mascota (models.Model):
	nombre = models.CharField(max_length=20)
	Edad  = models.CharField(max_length=5)
	comentario = models.CharField(max_length=200)
	propietario = models.ForeignKey(User, null=True, blank = True, on_delete = models.CASCADE)