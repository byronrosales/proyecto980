from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy

from administracion.forms import formularioRegistrar

def Home (request):
	return render (request, 'home.html',{})

#def AddUser (request):
#	return render (request, 'addUser.html',{})
class AddUser (CreateView):

	model = User
	template_name = "addUser.html"
	form_class = formularioRegistrar
	success_url = reverse_lazy('Home')

class AddCliente (CreateView):

	model = User
	template_name = "addCliente.html"
	form_class = formularioRegistrar
	success_url = reverse_lazy('Home')