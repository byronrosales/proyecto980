#from django.contrib.auth.models import User
from .models import User
from django.contrib.auth.forms import UserCreationForm

class formularioRegistrar (UserCreationForm):

	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
			'telefono',
			'edad',
			'rol',
		]
		